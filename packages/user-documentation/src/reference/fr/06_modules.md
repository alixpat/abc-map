<a name="modules"></a>

# Modules

Un module de traitement des données est un ensemble de fonctionnalités que vous pouvez charger et utiliser
depuis l'onglet "Traitement des données".

Vous pouvez utiliser des modules écrits par d'autres personnes ou écrire votre propre module.

<div class="alert alert-warning">
  <div class="mb-2">Vous devez vous assurer que vous pouvez faire confiance aux développeurs des modules   
  que vous utilisez.</div>
  <div>Les modules peuvent accéder à toutes les données de votre session Abc-Map.</div>
</div>

## Charger un module

<video controls src="./assets/load-module.mp4" preload="metadata"></video>

## Ecrire un module

Documentation par ici: [https://gitlab.com/abc-map/abc-map/-/blob/master/documentation/6_modules.md](https://gitlab.com/abc-map/abc-map/-/blob/master/documentation/6_modules.md)

<iframe class="youTubeIntegration" src="https://www.youtube.com/embed/mqt_CzSplJg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
