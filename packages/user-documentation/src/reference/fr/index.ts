/**
 * Copyright © 2021 Rémi Pace.
 * This file is part of Abc-Map.
 *
 * Abc-Map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Abc-Map is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with Abc-Map. If not, see <https://www.gnu.org/licenses/>.
 */

import toc from './00_toc.md';
import presentation from './01_presentation.md';
import basics from './02_basics.md';
import createMap from './03_create-map.md';
import importData from './04_import-data.md';
import registration from './05_registration.md';
import modules from './06_modules.md';
import mobileApp from './07_mobile-application.md';
import faq from './98_faq.md';
import help from './99_helpme.md';

const content = [presentation, basics, createMap, importData, registration, modules, mobileApp, faq, help];

export { toc, content };
