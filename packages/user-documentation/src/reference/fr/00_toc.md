<div class="toc-section">📚 Sommaire</div>

[Présentation](#presentation)
[Notions basiques](#basics)
[Créer une carte](#create-map)
[Import de données](#data-import)
[Inscription, connexion](#registration)
[Modules](#modules)
[Application mobile](#mobile-application)
[Questions fréquentes](#faq)
[🔥 A l'aide !](#helpme)
