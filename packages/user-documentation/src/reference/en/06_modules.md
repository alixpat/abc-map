<a name="modules"></a>

# Modules

A data processing module is a set of functionalities that you can load and use from the "Data processing" tab.

You can use modules written by other people or write your own module.

<div class="alert alert-warning">
  <div class="mb-2">You need to make sure that you can trust the developers of the modules you use.</div>
  <div>Modules can access all the data of your Abc-Map session.</div>
</div>

## Load a module

<video controls src="./assets/load-module.mp4" preload="metadata"></video>

## Write a module

See documentation here: [https://gitlab.com/abc-map/abc-map/-/blob/master/documentation/6_modules.md](https://gitlab.com/abc-map/abc-map/-/blob/master/documentation/6_modules.md)

<iframe class="youTubeIntegration" src="https://www.youtube.com/embed/mqt_CzSplJg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
