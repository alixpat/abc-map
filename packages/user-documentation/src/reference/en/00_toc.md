<div class="toc-section">📚 Table of content</div>

[Presentation](#presentation)
[Basic notions](#basics)
[Create a map](#create-map)
[Import data](#data-import)
[Registration, login](#registration)
[Modules](#modules)
[Mobile app](#mobile-application)
[Frequently asked questions](#faq)
[🔥 Help !](#helpme)
