# Docker compose deployment

This deployment is not actively used (we use Helm Chart for official instance).

Please open an issue in case of problem.

## Usage

You need to install [Docker and Docker Compose](https://docs.docker.com/get-started/):

```

  $ git clone https://gitlab.com/abc-map/abc-map
  $ cd abc-map/packages/infrastructure/docker-compose
  $ docker-compose up

```

Visit then [http://localhost:10082](http://localhost:10082).
